Generates drawing prompts from a list of words.

Written by [glub,](https://twitter.com/glubonit) invaluable technical
assistance by [Mozai,](https://twitter.com/mozai) greatly improved by
[ceequof.](https://twitter.com/ceequof)